import pickle
from color import color
from random import shuffle
from solve import makeSolution, saveSolution
from os.path import exists
import csv 
COMPASS = ["East", "South", "West", "North"]
COLORS = ["GREEN", "RED", "PURPLE", "BLUE"]


# load in social golfers solution
def fetchSolution(playerCount, gameCount):
    solutionPath = f"solutions/answer_{playerCount}_{gameCount}.pkl"
    if not exists(solutionPath):
        solution = makeSolution(playerCount, gameCount)
        if solution == None:
            raise Exception("Solution is INFEASIBLE")
        saveSolution(playerCount, gameCount, solutionPath)
    with open(solutionPath, 'rb') as file:
        return pickle.load(file)


def fetchPlayersTable(inputFile):
    # load in player list
    with open(inputFile, 'r', newline='') as file:
        data = csv.DictReader(file, delimiter=';')
        player_dicts = [row for row in data]
    return [p["name"] for p in player_dicts]   


def fetchPlayers(inputFile):
    return open(inputFile, 'r', newline='').read().split()


# print (colored) tournament seating [generic player names, eg "Player #1"]
def genericSolution(playerCount, gameCount):
    solution = fetchSolution(playerCount, gameCount)
    player_names = list(range(0,100))
    shuffle(player_names)
    for day, groups in solution.items():
        print(f"Round {day+1}:")
        for i,group in groups.items():
            print(f"Group {i+1}: ", end="")
            for seatIndex,playerIndex in group.items():
                pIndex = playerIndex[0]
                print(f"[{COMPASS[seatIndex]}] Player #{player_names[pIndex]+1}", end=" ")
            print()
        print()


# print (colored) tournament seating
def printSeating(playerNames, gameCount):
    solution = fetchSolution(len(playerNames), gameCount)

    # print player list
    for day, groups in solution.items():
        color()
        color("BOLD")
        color("UNDERLINE")
        print(f"Round {day+1}:")
        for i,group in groups.items():
            color()
            print(f"Group {i+1}: ", end="")

            for seatIndex,playerIndex in group.items():
                color(COLORS[seatIndex])
                pIndex = playerIndex[0]
                print(f"{COMPASS[seatIndex]} : {playerNames[pIndex]}", end=" ")
                color("ITALIC")
            print()
        print()
    color()


# save tournament seating as CSV file
def saveSeating(playerNames, gameCount, outputFile):
    solution = fetchSolution(len(playerNames), gameCount)
 
    with open(outputFile, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)

        # CSV header
        roundNumbers = []
        for i in range(1,gameCount+1):
            roundNumbers.append("")
            roundNumbers.append(i)
        writer.writerow(["Player ID", "Name"] + roundNumbers)
        writer.writerow(["", ""]              + ["Table", "Seat"] * gameCount)

        # Generate assignments (playerID[day] : {table,seat})
        assignments = [[None for _ in range(gameCount)] for _ in range(len(playerNames))]
        for day, groups in solution.items():
            for i,group in groups.items():
                for seatIndex,playerIndex in group.items():
                    pIndex = playerIndex[0]
                    table = i + 1
                    seat = COMPASS[seatIndex]
                    assignments[pIndex][day] = [table, seat]

        # write assignments
        for pIndex, allRounds in enumerate(assignments):
            playerName = playerNames[pIndex]
            playerID = pIndex + 1
            row = [playerID, playerName]
            for r in allRounds:
                row += r
            writer.writerow(row)


if __name__=="__main__":
    gameCount = 1
    players = fetchPlayers("input/players-bacon.csv")

    shuffle(players)
    printSeating(players, gameCount)
    saveSeating(players, gameCount, "output/Bacon2024.csv")