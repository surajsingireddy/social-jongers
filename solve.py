from ortools.sat.python import cp_model
from itertools import groupby, product, combinations
import pickle
import math
from color import color
import csv
from sys import argv


def groupby_keys(input_list, keylist):
    keyfunc = lambda x: tuple(x[k] for k in keylist)
    yield from groupby(sorted(input_list, key=keyfunc), key=keyfunc)


def makeSolution(playerCount, gameCount):
    # setup
    n_players = playerCount
    n_days = gameCount
    players_per_group = 4

    # intermediate values
    n_groups = n_players // players_per_group
    min_games_per_seat = n_days // players_per_group
    max_games_per_seat = math.ceil(n_days / players_per_group)
    players = list(range(n_players))
    days = list(range(n_days))
    groups = list(range(n_groups))
    seats = list(range(players_per_group))

    # setup SAT model
    model = cp_model.CpModel()
    variables = []
    player_vars = {}
    for player, day, group, seat in product(players, days, groups, seats):
        v_name = f"{player}_{day}_{group}_{seat}"
        the_var = model.NewBoolVar(v_name)
        variables.append({
            k: v
            for v, k in zip([v_name, player, day, group, seat, the_var],
                            ['Name', 'Player', 'Day', 'Group', 'Seat', 'CP_Var'])
        })
        player_vars[player, day, group, seat] = the_var

    # each player must be in a single group on each day
    for _, grp in groupby_keys(variables, ['Player', 'Day']):
        model.Add(sum(x['CP_Var'] for x in grp) == 1)

    # correct players per group (4 players at a table)
    for _, grp in groupby_keys(variables, ['Day', 'Group']):
        model.Add(sum(x['CP_Var'] for x in grp) == players_per_group)

    # each game must have one member in each seat (east/south/west/north)
    for _, grp in groupby_keys(variables, ['Day', 'Group', 'Seat']):
        model.Add(sum(x['CP_Var'] for x in grp) == 1)

    # players don't play in the same seat more often than necessary
    for _, grp in groupby_keys(variables, ['Player', 'Seat']):
        grp = list(grp)
        model.Add(sum(x['CP_Var'] for x in grp) <= max_games_per_seat)
        model.Add(sum(x['CP_Var'] for x in grp) >= min_games_per_seat)

        for idx, grp in groupby_keys(variables, ['Player']):
            east, south, west, north = [list(vrs) for _, vrs in groupby_keys(grp, ['Seat'])]
            
            model.Add(sum(x['CP_Var'] for x in east) == sum(x['CP_Var'] for x in north))
            model.Add(sum(x['CP_Var'] for x in west) == sum(x['CP_Var'] for x in south))

    # player-player interaction
    penalties = []
    for p1, p2 in combinations(players, r=2):
        players_together = []
        for day in days:
            for group in groups:
                together = model.NewBoolVar(f"M_{p1}_{p2}_{day}_{group}")
                players_together.append(together)
                p1g = sum(player_vars[p1, day, group, seat] for seat in seats)
                p2g = sum(player_vars[p2, day, group, seat] for seat in seats)
                model.Add(p1g + p2g - together <= 1)

        model.Add(sum(players_together) <= 1)

    # run solver
    solver = cp_model.CpSolver()
    solver.Solve(model)
    if ("status: INFEASIBLE" in solver.ResponseStats()):
        color("red")
        print(f"{playerCount} player / {gameCount} games SOLUTION is impossible")
        color()
        return None
    else:
        print(print(solver.ResponseStats()))

    # format/return solution
    solution = {}
    for var in variables:
        player, day, group, seat = var['Player'], var['Day'], var['Group'], var['Seat']
        solution[player, day, group, seat] = solver.Value(var['CP_Var'])

    days = sorted(set(x[1] for x in solution))
    groups = sorted(set(x[2] for x in solution))
    seats = sorted(set(x[3] for x in solution))
    answers = {}
    for day in days:
        answers[day] = {}
        for group in groups:
            answers[day][group] = {}
            for seat in seats:
                ans = [
                    k[0] for k, v in solution.items()
                    if k[1] == day and k[2] == group and k[3] == seat and v
                ]
                answers[day][group][seat] = sorted(ans)
    return answers


def saveSolution(playerCount, gameCount, solution):
    # save answers
    pickle_name = f"solutions/answer_{playerCount}_{gameCount}.pkl"
    with open(pickle_name, 'wb') as file:
        pickle.dump(solution, file)


def printSolution(solution):
    # print answers
    compass = ["East", "South", "West", "North"]
    colors = ["GREEN", "RED", "PURPLE", "BLUE"]
    for day, groups in solution.items():
        color()
        color("BOLD")
        color("UNDERLINE")
        print(f"Round {day+1}:")
        for i,group in groups.items():
            color()
            print(f"Group {i+1}: ", end="")

            for seatIndex,playerIndex in group.items():
                color(colors[seatIndex])
                pIndex = playerIndex[0]
                print(f"{compass[seatIndex]} : Player{pIndex}", end=" ")
            print()
        print()
    color()


if __name__=="__main__":
    solution = makeSolution(24,4)
    printSolution(solution)
    saveSolution(24,4,solution)
