# A file filled with general purpose utilities
# for IO with files, console, and web fetching
# by Shahrose Kasim (gitlab.com/users/RoseMaster3000)
################################################

from subprocess import DEVNULL, STDOUT, call
import os, json, sys, shutil, pickle
from urllib.request import urlopen
from time import time as epoch
import threading
from csv import writer as csvWriter

# TIME
EPOCH_MS = lambda: int(epoch() * 1000)
EPOCH = lambda: int(epoch())

# TERMINAL COLOR CODES
COLOR_ENABLED = True
COLORS = {
    "ENDC":         '\033[0m',
    "BOLD":         '\033[1m',
    "ITALIC":       '\033[3m',
    "UNDERLINE":    '\033[4m',
    "BLACK":        '\033[0;30m',
    "GREY":         '\033[1;30m',
    "GRAY":         '\033[1;30m',
    "RED":          '\033[0;31m',
    "LIGHT_RED":    '\033[1;31m',
    "GREEN":        '\033[0;32m',
    "LIGHT_GREEN":  '\033[1;32m',
    "BROWN":        '\033[0;33m',
    "YELLOW":       '\033[1;33m',
    "BLUE":         '\033[0;34m',
    "LIGHT_BLUE":   '\033[1;34m',
    "PURPLE":       '\033[0;35m',
    "LIGHT_PURPLE": '\033[1;35m',
    "CYAN":         '\033[0;36m',
    "LIGHT_CYAN":   '\033[1;36m',
    "LIGHT_GRAY":   '\033[0;37m',
    "WHITE":        '\033[1;37m' ,
}

VERT = '│'
HORZ = '─'
CROS = '┼┌┬┐└┴┘'
NUMS = "₀₁₂₃₄₅₆₇₈₉"

########################
# TERMINAL IO
########################

# TERMINAL output
def announce(message, width=40):
    return ANNOUNCE(message, width)
def ANNOUNCE(message, width=40):
    if width<0: width = len(message)*-width
    color("BOLD")
    printLine(width)
    print(message.upper().center(width))
    printLine(width)
    color("ENDC")
def printLine(width=40):
    print("─"*width)
def clear():
    clearScreen()
def clearScreen():
    if os.name == 'nt': shell('cls')
    else: shell('clear')

def clearLine(lineCount=1, width=None):
    for i in range(lineCount):
        sys.stdout.write("\033[K")
        sys.stdout.write("\033[F")
        if width!=None:
            if type(width)==str: width = len(width)
            print(" "*width)
            clearLine(lineCount=1, width=None)
def flush():
    sys.stdout.flush()
def shell(command, silent=False):
    if type(command)==str and "\"" not in command and "\'" not in command:
        command = command.split()
    if type(command)!=list:
        raise ValueError("mio.shell() expects a list strings")
    if silent:
        return call(command, stdout=DEVNULL, stderr=STDOUT)
    else:
        return call(command)


def color(colorKey=None):
    if COLOR_ENABLED:
        if colorKey == None:
            print(COLORS["ENDC"], end="")
        elif type(colorKey)==str:
            colorKey = colorKey.strip().upper()
            if colorKey.strip().upper() in COLORS:
                print(COLORS[colorKey], end="")


# prompt the user
def numInput(prompt, minimum=None, maximum=None, integer=False, failed=False):
    # ARGUMENT SANATATION
    if maximum==None: maximum = float('inf')
    if minimum==None: minimum = float('-inf')
    # input prompt (color red if previous attempt failed)
    if failed: color("LIGHT_RED")
    print(prompt, end="")
    if failed: color("ENDC")
    inp = input()

    # blank input (return default / re-display prompt with default)
    if inp =="":
        if minimum == float('-inf') and maximum > 0: default = 0
        else: default = minimum
        clearLine(1)
        print(f"{prompt} {default}")
        return default

    # INPUT VALIDATION
    try:
        # (if previous attempt failed, recolor default color)
        if failed:
            clearLine()
            print(prompt+inp)
        # cast to number
        if integer:
            numericInput = int(inp)
        else:
            numericInput = float(inp)
        # bound checker
        if numericInput > maximum:
            raise Exception("Input value too high")
        elif numericInput < minimum:
            raise Exception("Input value too low")
        else:
            return numericInput
    # on invalid input... (recursively prompt again)
    except:
        clearLine(width=len(prompt+inp))
        return numInput(prompt,
            failed=True,
            maximum=maximum,
            minimum=minimum,
            integer=integer)

# using a list, print numbered menu
def menu(optionList, reverse=False):
    if reverse: optionList = optionList[::-1]
    # generate prompt
    menuLines = []
    width = len(optionList)//10
    for i,option in enumerate(optionList):
        menuLine = "{:>"+str(width)+"}. {}"
        if callable(option): option = option.__name__
        menuLines.append(menuLine.format(i+1, option))
    # display prompt
    print("\n".join(menuLines))
    menuPrompt = f"Select an option (1-{len(optionList)}): "
    menuSelection = -1 + numInput(menuPrompt,
        minimum=1,
        maximum=len(optionList),
        integer=True
        )
    # return item from list
    return optionList[menuSelection]


# print progress to terminal; returns message length > externally call mio.clearLine(1, msgLength)
def printProgress(level, currentName, currentNumber, currentList, clearLine=True):
    if type(currentList)==list: currentList = len(currentList)
    if type(currentNumber) in [int,float]: currentNumber += 1
    indentation = level * "    "
    progressString = f"{indentation}Processing {currentName:<18}  -  {currentNumber} / {currentList}"
    print(progressString)
    return len(progressString)

# generates a numbered/unique file name
def generateFileName(prefix, folder="."):
    if not os.path.isdir(folder): os.mkdir(folder)
    allFiles = os.listdir(folder)
    maxNumber = -1
    for file in allFiles:
        if file.startswith(prefix):
            if "." in file:
                number = int(file.split(".")[0].split("_")[-1])
            elif "_" in file:
                number = int(file.split("_")[-1])
            if number > maxNumber:
                maxNumber = number
    maxNumber += 1
    return os.path.join(folder, f"{prefix}_{maxNumber}")

########################
# FILE IO
########################

# WRITE files
def writeText(filePath, content):
    with open(filePath, "w") as file:
        file.write(content)
def writeJson(filePath, data, indent=4):
    if not filePath.endswith(".json"): filePath += ".json"
    if type(data)==str: data = json.loads(data)
    jsonString = json.dumps(data, indent=indent, sort_keys=False)
    with open(filePath,"w") as file:
        file.write(jsonString)
def renameFile(filePath, newName):
    os.rename(filePath, newName)
def moveFile(fileStart, fileEnd):
    shutil.move(fileStart, fileEnd)
def writeCSV(filePath, data, delimiter=","):
    with open(filePath, "w") as file:
        write = csvWriter(file)
        write.writerows(data)
def writePickle(filePath, data):
    with open(filePath, 'wb') as f:
        pickle.dump(data, f)


# READ files
def readPickle(filePath, fallback=None):
    if not isFile(filePath): return fallback
    with open(filePath, 'rb') as f:
        return pickle.load(f)
def readJson(filePath):
    with open(filePath, "r") as file:
        return json.load(file)
def parseJson(jsonString):
    return json.loads(jsonString)
def readText(filePath):
    with open(filePath, "r") as file:
        return file.read()
def readData(fileName, outputType=str):
    data=[]
    with open(fileName) as f:
        for line in f:
            if outputType==int:
                data.append(int(line))
            elif outputType==float:
                data.append(float(line))
            else:
                data.append(line)
    return data
def readCSV(filePath, delimiter=",", header=False):
    with open(filePath, "r") as file:
        data = file.read()
        lineList = data.split("\n")
    if header==True:
        headerList = lineList.pop(0).split(delimiter)
        headerList = [h.strip() for h in headerList]
        goodData = []
        for line in lineList:
            entry = {}
            row = line.split(delimiter)
            row = [cell.strip() for cell in row]
            if len(row)!=len(headerList): continue
            for i,key in enumerate(headerList):
                entry[key] = row[i]
            goodData.append(entry)
        return goodData
    else:
        return [line.split(delimiter) for line in lineList]





########################
# FILESYSTEM IO
########################
def isFile(filePath):
    return os.path.isfile(filePath)
def isDirectory(dirPath):
    return os.path.isdir(dirPath)


# DELETE files/folders
def deleteFile(filePath):
    os.remove(filePath)
def deleteDirectory(*dirPath):
    dirPath = joinPath(*dirPath)
    if isDirectory(dirPath):
        shutil.rmtree(dirPath)
def makeDirectory(*dirPath):
    dirPath = joinPath(*dirPath)
    if not isDirectory(dirPath):
        os.mkdir(dirPath)


# LIST folders in directory
def listFolders(*dirPath):
    dirPath = joinPath(*dirPath)
    for file in listDirectory(dirPath):
        path = joinPath(dirPath, file)
        if not isFile(path):
            yield path

# LIST files in directory
def listFiles(*dirPath):
    dirPath = joinPath(*dirPath)
    for file in listDirectory(dirPath):
        path = joinPath(dirPath, file)
        if isFile(path):
            yield path
# LIST files in directory + files in child sub-directories
def listFilesDeep(*dirPath):
    dirPath = joinPath(*dirPath)
    return [os.path.join(root, name)
            for root, dirs, files in os.walk(dirPath)
            for name in files]

# LIST files+folders in directory
def listDirectory(*dirPath):
    dirPath = joinPath(*dirPath)
    return os.listdir(dirPath)

# get number of subfolders in folder
def countFolders(*dirPath):
    dirPath = joinPath(*dirPath)
    folders = listFolders(path) 
    return len(list(folders))
# get number of files in folder
def countFiles(dirPath):
    dirPath = joinPath(*dirPath)
    files = listFiles(path) 
    return len(list(files))


# join list of folder into path string
def joinPath(*args):
    if len(args)==0:
        return ""
    elif args[0]==None:
        return os.path.join(*args[1:])
    else:
        return os.path.join(*args)
# clean up string
def sanatizePhrase(phrase):
    for char in list("“”.,\'\"/()[]"):
        phrase = phrase.replace(char," ")
    phrase = " ".join(phrase.split())
    phrase = phrase.title()
    return phrase


########################
# WEB IO
########################
def isWebsite(website):
    try:
        return (urlopen(website).status == 200)
    except:
        return False

# READ webpages
def requestText(website):
    return urlopen(website).read().decode('utf8')
def requestJson(website):
    data = requestText(website)
    if data==None: return None
    return parseJson(data)


########################
# TIME MATH
########################

def time(timeRangeString):
    times = timeRangeString.split("-")
    a = Time.fromString(times[0])
    b = Time.fromString(times[1])
    return a - b


class Time:
    def __init__(self, hour:int, minute:int):
        self.hours = hour
        self.minutes = minute
        self.military = hour*60+minute

    @classmethod
    def fromString(cls, timeString):
        if ":" in timeString:
            h,m = timeString.split(":")
            return cls( int(h), int(m) )
        else:
            return cls( int(timeString), 0 )

    def __gt__(self, other):
        if type(other)!=Time: raise TypeError
        return self.military > other.military

    def __repr__(self):
        return f"{self.hours}:{self.minutes}"

    def __sub__(self, other):
        if type(other)!=Time: raise TypeError

        # am / pm crossover (convert to military)
        if other < self: other.hours += 12

        deltaHour = other.hours - self.hours
        deltaMinute = other.minutes - self.minutes
        if deltaMinute < 0:
            deltaHour -= 1
            deltaMinute += 60

        return round(deltaHour + deltaMinute/60, 3)



########################
# OTHER
########################

# flatten any list -> 1D array
def flatten(inList):
    outList = []
    for item in inList:
        if type(item)==list:
            outList += flatten(item)
        else:
            outList.append(item)
    return outList

# run a function on a separate thread
def startDaemon(process):
    t = threading.Thread(target=process)
    t.daemon = True
    t.start()

# send an notification popup Zenity (GTK Linux notify)
def popupInfo(title="Listen Up!", message="An error occurred", level=1):
    alert = ["info", "warning", "error"][level]
    command = [
        'zenity',
        f'--{alert}',
        f'--text=\"{message}\"',
        f'--title=\"{title}\"'
    ]
    shell(command, silent=True)

# send an Y/N popup Zenity (GTK Linux alert)
def popupQuestion(question):
    command = f'zenity --question --text=\"\"{question}\"\"'
    command = [
        'zenity',
        f'--question',
        f'--text=\"{question}\"'
    ]
    response = shell(command, silent=True)
    if response == 0:
        return True
    elif response==1:
        return False
    else:
        return None


if __name__=="__main__":
    # requestJson("https://api.weather.gov/")

    assert time('2:30 - 11:00') == 8.5
    assert time('3:45 - 6:50') == 3.083

# 3:40 (self)
# 6:50 (other) # normal

# 11:00 (self)
# 13:20  (other) #am / pm crossover



# ZENETY POPUP TYPES
# --calendar
# --entry
# --error
# --info
# --file-selection
# --list               <<<< def popupList(...)
# --notification
# --progress
# --question
# --warning
# --scale
# --text-info
# --color-selection
# --password
# --forms
# --display=DISPLAY